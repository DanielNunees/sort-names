﻿using System.Collections.Generic;
using System.IO;
using GlobalXNameSorter.Interfaces;

namespace GlobalXNameSorter
{
    /// <summary>
    /// Contains methods for performing basic file read and write functions.
    /// </summary>
    public class FileHandler : IFileIO
    {
        /// <summary>
        /// Create List of items from a .txt file
        /// </summary>
        /// <returns>
        /// A list of items
        /// </returns>
        public List<string> Read(string FileUrl)
        {
            var reader = new StreamReader(FileUrl);
            var names = new List<string>();
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                names.Add(line);
            }
            return names;
        }

        /// <summary>
        /// Write a txt file with a given List strings
        /// </summary>
        public void Write(List<string> items, string fileName, bool overwrites)
        {
            using (StreamWriter sw = new StreamWriter(fileName, overwrites))
            {
                sw.AutoFlush = true;
                items.ForEach((item) =>
               {
                   sw.WriteLine(item);
               });
                sw.Flush();
            }
        }
    }
}
