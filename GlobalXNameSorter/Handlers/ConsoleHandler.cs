﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GlobalXNameSorter
{
    public class ConsoleHandler
    {
        /// <summary>
        /// Print a List<string> on the console, line by line
        /// </summary>
        public void PrintList(List<string> list)
        {
            list.ForEach(item => { Console.WriteLine(item + "\n"); });
        }

        /// <summary>
        /// Clears the console
        /// </summary>
        public void Clear()
        {
            Console.Clear();
        }

        /// <summary>
        /// Print basic info about the list
        ///  - List count
        ///  - Duplicated items
        /// </summary>
        public void ListInfo(List<string> list)
        {
            var count = list.Count;
            var duplicateItems = list.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => new { Element = y.Key, Counter = y.Count() })
              .ToList();

            Console.WriteLine("Items in the list: " + count + "\n");
            if (duplicateItems.Count > 0)
            {
                Console.WriteLine("Duplicated Items:");
                foreach (var duplicateItem in duplicateItems)
                {
                    Console.WriteLine(duplicateItem);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Pring the company's logo
        /// </summary>
        public void PrintLogo()
        {
            string header = @"
               ________      __          ___  __
              / ____/ /___  / /_  ____ _/ / |/ /
             / / __/ / __ \/ __ \/ __ `/ /|   / 
            / /_/ / / /_/ / /_/ / /_/ / //   |  
            \____/_/\____/_.___/\__,_/_//_/|_|  
                                    
            ";
            Console.WriteLine(header + "\n");
        }
    }
}
