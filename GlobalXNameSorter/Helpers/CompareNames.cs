﻿using System.Collections.Generic;
using System.Linq;
using GlobalXNameSorter.Interfaces;

namespace GlobalXNameSorter
{
    public class CompareNames : ICompare
    {
        /// <summary>
        /// Compare a list of names a sort a list of names
        /// First by lastname then by first name
        /// </summary>
        /// <return>
        /// The sorted list
        /// </return>
        public List<Names> Compare(List<Names> names) => names.OrderBy(x => x._lastName).ThenBy(y => y._firstName).ToList();
    }
}
