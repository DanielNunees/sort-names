﻿using System.Collections.Generic;
using System.Linq;

namespace GlobalXNameSorter
{
    public class Names
    {
        public string _firstName { get; }
        public string _lastName { get; }
        private string _givenNames { get; }

        public Names(string firstName, string lastName, string givenNames)
        {
            _firstName = firstName;
            _lastName = lastName;
            _givenNames = givenNames;
        }

        public Names()
        {
        }

        /// <summary>
        /// Return the full name
        /// </summary>
        public string FullName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_givenNames))
                    return $"{_firstName} {_lastName}";
                else return $"{_firstName} {_givenNames} {_lastName}";
            }
        }

        /// <summary>
        /// Create List<Names> from List<String>
        /// </summary>
        /// <returns>
        /// List<Names>
        /// </returns>
        public List<Names> CreateList(List<string> names)
        {
            var namesList = new List<Names>();
            names.ForEach(name =>
            {
                var firstName = name.Split(" ").First();
                var lastName = name.Split(" ").Last();
                var givenNames = string.Join(" ", name.Split(" ").Skip(1).SkipLast(1).ToArray());
                var newName = new Names(firstName, lastName, givenNames);
                namesList.Add(newName);
            });
            return namesList;
        }

        /// <summary>
        /// Create List<string> from List<Names>
        /// </summary>
        /// <returns>
        /// List<Names>
        /// </returns>
        public List<string> ToArray(List<Names> names)
        {
            var genericList = new List<string>();
            names.ForEach(name =>
            {
                genericList.Add(name.FullName);
            });
            return genericList;
        }
    }
}
