﻿using System.Collections.Generic;

namespace GlobalXNameSorter.Interfaces
{
    public interface ICompare
    {
        List<Names> Compare(List<Names> names);
    }
}
