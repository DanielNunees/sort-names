﻿using System.Collections.Generic;

namespace GlobalXNameSorter.Interfaces
{
    public interface IFileIO
    {
        List<string> Read(string FileUrl);
        void Write(List<string> names, string fileName, bool overwrites = false);
    }
}
