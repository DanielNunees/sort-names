﻿using System;

namespace GlobalXNameSorter
{
    public class NamesSorter
    {

        public static string _unsortedFileUrl = @"./unsorted-names-list.txt";
        public static string _sortedFileUrl = @"./sorted-names-list.txt";
        static void Main(string[] args)
        {
            try
            {
                //get file in disk or as a argument
                string fileUrl = args.Length == 0 ? _unsortedFileUrl : args[0];
                var names = new Names();
                var compareNames = new CompareNames();
                var fileHandler = new FileHandler();
                var consoleHandler = new ConsoleHandler();
                var nameSorter = new NamesSorter();

                nameSorter.execute(fileUrl, names, compareNames, fileHandler, consoleHandler);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Execute all steps to sort, print and write 
        /// a txt file with the sorted list
        /// </summary>
        public void execute(string fileUrl, Names names, CompareNames compareNames, FileHandler fileHandler, ConsoleHandler consoleHandler)
        {
            var namesListFromFile = fileHandler.Read(fileUrl);
            var namesList = names.CreateList(namesListFromFile);
            var sortedNames = compareNames.Compare(namesList);

            consoleHandler.Clear();
            consoleHandler.PrintLogo();
            consoleHandler.ListInfo(names.ToArray(sortedNames));
            ((Interfaces.IFileIO)fileHandler).Write(names.ToArray(sortedNames), _sortedFileUrl);
            consoleHandler.PrintList(names.ToArray(sortedNames));
        }
    }
}
