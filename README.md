﻿
# GlobalX 
## Name-sorter project

Build a name sorter. Given a set of names, order that set first by last name, then by any given names the person may have. A
name must have at least 1 given name and may have up to 3 given names.


### Example Usage
Given a a file called unsorted-names-list.txt containing the following list of names;

    Janet Parsons
    Vaughn Lewis
    Adonis Julius Archer
    Shelby Nathan Yoder
    Marin Alvarez
    London Lindsey
    Beau Tristan Bentley
    Leo Gardner
    Hunter Uriah Mathew Clarke
    Mikayla Lopez
    Frankie Conner Ritter

Executing the program in the following way;

    name-sorter ./unsorted-names-list.txt

Should result the sorted names to screen;

    Marin Alvarez
    Adonis Julius Archer
    Beau Tristan Bentley
    Hunter Uriah Mathew Clarke
    Leo Gardner
    Vaughn Lewis
    London Lindsey
    Mikayla Lopez
    Janet Parsons
    Frankie Conner Ritter
    Shelby Nathan Yoder

# Documentation
The project adhered to the SOLID principles, with classes focusing on one and only one task. Also, even though the project's aim is to deal with names, the majority of methods and classes were designed to operate with any given generic `List<string>`.

## Folder Structure

    - NamesSorter.cs
    - Names.cs
    - Handlers\
    	- ConsoleHandler.cs
    	- FileHandler.cs
    - Helpers\
    	- CompareNames.cs
    - Interfaces\
    	- ICompare.cs
    	- IFileIO.cs
## NamesSorter.cs (main class)
Create all the object necessaries to run and execute the project. The `Main()` method will be called when the project is executed.

## Names.cs
Simplified model. You can create an instance of the Names class and use to manipulate the `_firstname`, `_lastname` and `_givenNames` separately.

### Methods

    List<Names> createList(List<string> names) 

Receive a `List<String>` and it converts to a `List<Names>` with access  to each part of the name with getters and setters.

    List<string> ToArray(List<Names> names)
It receive a `List<Names>` and it converts to a generic `List<String>`, offering more generic actions.

## Handlers\ConsoleHandler.cs
A handler that implements the `System.Console()` functions with proper implementations to the project need.

### Methods
    void PrintList(List<string> list)
Print a List<string> on the console, line by line.

    void Clear()
Clears the console.

    void ListInfo(List<string> list)
Print basic info about the list
-> List count
-> Duplicated items

    void PrintLogo()
Print the company's logo.

## Handlers/FileHandler.cs
This class Contains methods for performing basic file read and write functions.

### Methods

    List<string> Read(string FileUrl)
Create List of items from a .txt file.

    void Write(List<string> items, string fileName, bool overwrites)
Write a txt file with a given `List<string>`.

## Helpers\CompareNames
The class responsible to compare the names in a given list and order by a pre determined set of rules.

### Methods

    List<Names> compare(List<Names> names)
Compare a list of names a sort a list of names first by `_lastname` then by `_firstname`

## Unit Tests
It consists of 8 tests.
Test the basic functionality of the project (Not really creative here).

### Methods

    IList createTestList(string returnListType)
Read a file an create a list for tests.

    public void FileReadTest()
Test the `FileHandler.Read()` method, if it is capable to read an txt file and return a `List<string>`.

    public void EmptyFileReadTest()
Test the `FileHandler.Read()` method, against a possible empty txt file and still should return a `List<string>`.

    public void CreateNamesListTest()

Test the `Names.CreateList()` method, with a `List<string>`  and see if it can convert it to a `List<Names>`.

    public void CreateNamesWithEmptyListTest()
Test the `Names.CreateList()` method, against a possible empty txt file and still should return an empty `List<Names>`.

    public void CreateStringFromNamesListTest()
Test the `Names.ToArray()` method, with a `List<Names>`  and see if it can convert it to a `List<string>`.

    public void CreateStringListFromEmptyNamesListTest()
Test the `Names.ToArray()` method, against a possible empty txt file and still should return an empty `List<string>`.

    public void CompareNamesWithEmptyListTest()
Test the `CompareNames.Compare()` method, against a possible empty txt file and still should return an empty `List<Names>`.

    public void CompareNamesTest()
Test the `CompareNames.Compare()` method, with an `List<string>` and it should return a `List<Names>` with the items sorted.
