﻿using System.Collections;
using System.Collections.Generic;
using GlobalXNameSorter;
using Xunit;

namespace UnitTest
{
    /// <summary>
    /// Unit test for the NameSorter Project.
    /// </summary>
    public class NamesSorterUnitTest
    {
        private string fileUrl = @"./unsorted-names-list.txt";
        private string emptyfileUrl = @"./empty-unsorted-names-list.txt";

        public IList CreateTestList(string returnListType)
        {
            var fileHandler = new FileHandler();
            var names = new Names();
            var namesListFromFile = fileHandler.Read(fileUrl);
            switch (returnListType)
            {
                case "string":
                    return namesListFromFile;
                case "namesList":
                    return names.CreateList(namesListFromFile);
                default:
                    return namesListFromFile;
            }
        }

        [Fact]
        public void FileReadTest()
        {
            var fileHandler = new FileHandler();
            var namesListFromFile = fileHandler.Read(fileUrl);
            bool result = (namesListFromFile.GetType().Equals(typeof(List<string>)));
            Assert.True(result, "Should be a String List");
        }

        [Fact]
        public void EmptyFileReadTest()
        {
            var fileHandler = new FileHandler();
            var namesListFromFile = fileHandler.Read(emptyfileUrl);
            bool result = namesListFromFile.GetType().Equals(typeof(List<string>));
            Assert.True(result, "Should be a String List");
        }

        [Fact]
        public void CreateNamesWithEmptyListTest()
        {
            var names = new Names();
            var namesList = names.CreateList(new List<string>());
            bool result = namesList.GetType().Equals(typeof(List<Names>));
            Assert.True(result, "Should be a Names List");
        }

        [Fact]
        public void CreateNamesListTest()
        {
            var names = new Names();
            var namesList = names.CreateList((List<string>)CreateTestList("string"));
            bool result = namesList.GetType().Equals(typeof(List<Names>));
            Assert.True(result, "Should be a Names List");
        }

        [Fact]
        public void CreateStringFromNamesListTest()
        {
            var names = new Names();
            var namesList = names.ToArray((List<Names>)CreateTestList("namesList"));
            bool result = namesList.GetType().Equals(typeof(List<string>));
            Assert.True(result, "Should be a string List");
        }

        [Fact]
        public void CreateStringListFromEmptyNamesListTest()
        {
            var names = new Names();
            var namesList = names.ToArray(new List<Names>());
            bool result = namesList.GetType().Equals(typeof(List<string>));
            Assert.True(result, "Should be a string List");
        }

        [Fact]
        public void CompareNamesWithEmptyListTest()
        {
            var compareNames = new CompareNames();
            var sortedNames = compareNames.Compare(new List<Names>());
            bool result = (sortedNames.GetType().Equals(typeof(List<Names>)));
            Assert.True(result, "Should be a Names List");
        }


        [Fact]
        public void CompareNamesTest()
        {
            var compareNames = new CompareNames();
            var sortedNames = compareNames.Compare((List<Names>)CreateTestList("namesList"));
            bool result = (sortedNames.GetType().Equals(typeof(List<Names>)));
            Assert.True(result, "Should be a Names List");
        }
    }
}
